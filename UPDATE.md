# 1.0.0
## alpha.1
- 第一个内测版本。

## alpha.2
- 修复objects常量模板的`toString`异常问题。
- 追加常量模板类 `NekoObjects` 。

## alpha.3
- 追加 `lazy` （属性值懒加载）相关的neko，以及对应的builder。（`CatCodeUtil#getLazyNekoBuilder`）
